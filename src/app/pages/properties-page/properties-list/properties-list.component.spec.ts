import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement, Input } from '@angular/core';
import { MatList } from '@angular/material/list';
import { MatRippleModule } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Store, MemoizedSelector } from '@ngrx/store';

import * as fromRoot from '@store/reducers/property.reducer';

import { IProperty } from '@models/property';
import { Actions } from '@models/dialog';

import { storeValueMock } from '@shared/mocks/properties';
import { PropertyModalComponent } from '@shared/components/property-modal/property-modal.component';

import { PropertiesListComponent } from './properties-list.component';

class MdDialogMock {
  open() {}
}

describe('PropertiesListComponent', () => {
  let component: PropertiesListComponent;
  let fixture: ComponentFixture<PropertiesListComponent>;
  let debugElement: DebugElement;
  let nativeElement;
  let dialog: MatDialog;
  let store: any;
  let mockGetAllEventsSelector: MemoizedSelector<
    string,
    { description: string; id: number; name: string }[]
  >;

  @Component({ selector: 'lop-property', template: '' })
  class PropertyComponent {
    @Input() property: IProperty;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PropertiesListComponent, PropertyComponent, MatList],
      providers: [
        provideMockStore(),
        { provide: MatDialog, useClass: MdDialogMock },
      ],
      imports: [MatRippleModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertiesListComponent);
    dialog = TestBed.inject(MatDialog);
    store = TestBed.inject(Store);
    component = fixture.componentInstance;
    nativeElement = fixture.nativeElement;
    debugElement = fixture.debugElement;

    mockGetAllEventsSelector = store.overrideSelector(
      'properties',
      storeValueMock.properties
    );
    fixture.detectChanges();
  });

  it('should create the PropertiesListComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should call createProperty on create button clicked', () => {
    spyOn(dialog, 'open').and.callThrough();
    spyOn(component, 'createProperty').and.callThrough();

    const addButton = debugElement.query(
      By.css('button[data-qa=create-button]')
    );
    addButton.triggerEventHandler('click', null);

    expect(component.createProperty).toHaveBeenCalled();
    expect(dialog.open).toHaveBeenCalledWith(PropertyModalComponent, {
      data: {
        action: Actions.CREATE,
      },
    });
  });
});
