import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { PropertyModalComponent } from '@shared/components/property-modal/property-modal.component';

import { select, Store } from '@ngrx/store';
import { State } from '@store/reducers/property.reducer';
import { Actions } from '@models/dialog';
import { Observable } from 'rxjs';
import { IProperty } from '@models/property';

@Component({
  selector: 'lop-properties-list',
  templateUrl: './properties-list.component.html',
  styleUrls: ['./properties-list.component.scss'],
})
export class PropertiesListComponent implements OnInit {
  properties$: Observable<IProperty[]>;

  constructor(private matDialog: MatDialog, private store: Store<State>) {}

  ngOnInit() {
    this.properties$ = this.store.pipe(select('properties'));
  }

  createProperty() {
    this.matDialog.open(PropertyModalComponent, {
      data: { action: Actions.CREATE },
    });
  }
}
