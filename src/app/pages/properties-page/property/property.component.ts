import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { IProperty } from '@models/property';

import { ConfirmationModalComponent } from '@shared/components/confirmation-modal/confirmation-modal.component';
import { PropertyModalComponent } from '@shared/components/property-modal/property-modal.component';
import { Actions } from '@models/dialog';

@Component({
  selector: 'lop-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss'],
})
export class PropertyComponent {
  @Input() property: IProperty;

  constructor(private matDialog: MatDialog) {}

  updateProperty() {
    this.matDialog.open(PropertyModalComponent, {
      data: {
        action: Actions.UPDATE,
        property: this.property,
      },
    });
  }

  removeProperty() {
    this.matDialog.open(ConfirmationModalComponent, {
      data: {
        property: this.property,
      },
    });
  }
}
