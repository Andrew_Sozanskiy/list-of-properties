import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatDialog } from '@angular/material/dialog';
import { MatRippleModule } from '@angular/material/core';
import { MatListItem } from '@angular/material/list';

import { ConfirmationModalComponent } from '@shared/components/confirmation-modal/confirmation-modal.component';
import { PropertyModalComponent } from '@shared/components/property-modal/property-modal.component';
import { fakePropertyPayload } from '@shared/mocks/properties';

import { PropertyComponent } from './property.component';
import { DebugElement } from '@angular/core';
import { Actions } from '@models/dialog';
import { By } from '@angular/platform-browser';

class MdDialogMock {
  open() {}
}

describe('PropertyComponent', () => {
  let component: PropertyComponent;
  let fixture: ComponentFixture<PropertyComponent>;
  let dialog: MatDialog;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PropertyComponent, MatListItem],
      providers: [{ provide: MatDialog, useClass: MdDialogMock }],
      imports: [MatRippleModule],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(PropertyComponent);
        dialog = TestBed.inject(MatDialog);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        component.property = fakePropertyPayload;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call updateProperty', () => {
    spyOn(dialog, 'open').and.callThrough();
    spyOn(component, 'updateProperty').and.callThrough();

    const updateButton = debugElement.query(
      By.css('button[data-qa=update-property-button]')
    );
    updateButton.triggerEventHandler('click', null);

    expect(component.updateProperty).toHaveBeenCalled();
    expect(dialog.open).toHaveBeenCalledWith(PropertyModalComponent, {
      data: {
        action: Actions.UPDATE,
        property: fakePropertyPayload,
      },
    });
  });

  it('should call removeProperty', () => {
    spyOn(dialog, 'open').and.callThrough();
    spyOn(component, 'removeProperty').and.callThrough();

    const removeButton = debugElement.query(
      By.css('button[data-qa=remove-property-button]')
    );
    removeButton.triggerEventHandler('click', null);

    expect(component.removeProperty).toHaveBeenCalled();
    expect(dialog.open).toHaveBeenCalledWith(ConfirmationModalComponent, {
      data: {
        property: fakePropertyPayload,
      },
    });
  });
});
