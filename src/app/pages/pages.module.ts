import { NgModule } from '@angular/core';

import { PagesRoutingModule } from '@pages/pages-routing.module';

import { PagesComponent } from './pages.component';
import { PropertiesPageComponent } from './properties-page/properties-page.component';
import { PropertiesListComponent } from './properties-page/properties-list/properties-list.component';
import { PropertyComponent } from './properties-page/property/property.component';
import { SharedModule } from '@shared/shared.module';

const MODULES = [SharedModule, PagesRoutingModule];

const COMPONENTS = [
  PagesComponent,
  PropertiesPageComponent,
  PropertiesListComponent,
  PropertyComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  providers: [],
})
export class PagesModule {}
