import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Store } from '@ngrx/store';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { State } from '@store/reducers/property.reducer';
import { addOne, updateOne } from '@store/actions/property.action';

import { Actions, IUserAction } from '@models/dialog';

@Component({
  selector: 'lop-property-modal',
  templateUrl: './property-modal.component.html',
  styleUrls: ['./property-modal.component.scss'],
})
export class PropertyModalComponent implements OnInit {
  propertyForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IUserAction,
    public dialogRef: MatDialogRef<PropertyModalComponent>,
    public formBuilder: FormBuilder,
    private store: Store<State>
  ) {}

  get title(): string {
    switch (this.data.action) {
      case Actions.CREATE:
        return 'Create new property';
      case Actions.UPDATE:
        return 'Update property';
      default:
        return '';
    }
  }

  ngOnInit() {
    this.initForm();
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    const { name, description } = this.propertyForm.value;
    const id = Math.floor(Math.random() * 1000000) + 1;

    switch (this.data.action) {
      case Actions.CREATE:
        this.store.dispatch(
          addOne({
            payload: {
              id,
              name,
              description,
            },
          })
        );
        break;
      case Actions.UPDATE:
        const { id: propertyId } = this.data.property || {};
        this.store.dispatch(
          updateOne({
            payload: {
              id: propertyId || 0,
              name,
              description,
            },
          })
        );
        break;
    }
    this.dialogRef.close();
  }

  initForm() {
    const { name, description } = this.data.property || {};
    this.propertyForm = this.formBuilder.group({
      name: [name || ''],
      description: [description || '', [Validators.required]],
    });
  }
}
