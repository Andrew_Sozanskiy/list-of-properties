import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatRippleModule } from '@angular/material/core';

import { Actions, IUserAction } from '@models/dialog';
import { fakePropertyPayload } from '../../mocks/properties';
import { PropertyModalComponent } from './property-modal.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { Store } from '@ngrx/store';

class MdDialogMock {
  close() {}
}

class StoreMock {
  dispatch(action: any) {}
}

const editModeValues: IUserAction = {
  action: Actions.UPDATE,
  property: fakePropertyPayload,
};

const AddEventDialogData: IUserAction = {
  action: Actions.CREATE,
};

describe('PropertyModalComponent', () => {
  let component: PropertyModalComponent;
  let fixture: ComponentFixture<PropertyModalComponent>;
  let debugElement: DebugElement;
  let nativeElement;
  let dialog: MatDialogRef<any>;
  let saveButton: DebugElement;
  let store: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PropertyModalComponent],
      providers: [
        { provide: Store, useClass: StoreMock },
        { provide: MatDialogRef, useClass: MdDialogMock },
        { provide: MAT_DIALOG_DATA, useValue: AddEventDialogData },
      ],
      imports: [
        MatRippleModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
      ],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(PropertyModalComponent);
        dialog = TestBed.inject(MatDialogRef);
        store = TestBed.inject(Store);
        component = fixture.componentInstance;
        nativeElement = fixture.nativeElement;
        debugElement = fixture.debugElement;
        component.ngOnInit();
        fixture.detectChanges();
        saveButton = debugElement.query(
          By.css('button[data-qa=save-dialog-button]')
        );
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('EventInfoModalComponent OnInit', () => {
    spyOn(component, 'initForm').and.callThrough();

    component.ngOnInit();

    expect(component.initForm).toHaveBeenCalled();
  });

  it('on save when dialog action create', () => {
    spyOn(component, 'save').and.callThrough();
    spyOn(dialog, 'close').and.callThrough();

    saveButton.triggerEventHandler('click', null);
    expect(dialog.close).toHaveBeenCalled();
  });

  it('on save when dialog action update', () => {
    spyOn(component, 'save').and.callThrough();
    spyOn(dialog, 'close').and.callThrough();
    spyOn(store, 'dispatch').and.callThrough();

    component.data = editModeValues;
    saveButton.triggerEventHandler('click', null);

    const { id } = component.data.property || {};
    const name = component.propertyForm.get('name')?.value;
    const description = component.propertyForm.get('description')?.value;

    expect(store.dispatch).toHaveBeenCalledWith({
      payload: {
        id,
        name,
        description,
      },
      type: '[Properties] Update One',
    });
    expect(dialog.close).toHaveBeenCalled();
  });

  it('initForm when dialog action edit', () => {
    spyOn(component.formBuilder, 'group').and.callThrough();

    component.data = editModeValues;
    component.initForm();

    const { name } = editModeValues.property || {};

    expect(component.propertyForm.get('name')?.value).toEqual(name);

    const { description } = editModeValues.property || {};
    expect(component.propertyForm.get('description')?.value).toEqual(
      description
    );
  });

  it('should call close', () => {
    spyOn(dialog, 'close').and.callThrough();

    const closeButton = debugElement.query(
      By.css('button[data-qa=close-dialog-button]')
    );
    closeButton.triggerEventHandler('click', null);

    expect(dialog.close).toHaveBeenCalled();
  });
});
