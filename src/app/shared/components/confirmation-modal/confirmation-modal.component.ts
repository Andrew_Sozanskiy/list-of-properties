import { Component, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Store } from '@ngrx/store';

import { State } from '@store/reducers/property.reducer';
import { removeOne } from '@store/actions/property.action';

import { IProperty } from '@models/property';

@Component({
  selector: 'lop-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss'],
})
export class ConfirmationModalComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { property: IProperty },
    public dialogRef: MatDialogRef<ConfirmationModalComponent>,
    private store: Store<State>
  ) {}

  close() {
    this.dialogRef.close();
  }

  remove() {
    this.store.dispatch(removeOne({ payload: this.data.property }));
    this.dialogRef.close();
  }
}
