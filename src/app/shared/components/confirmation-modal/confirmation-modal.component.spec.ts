import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialogModule,
} from '@angular/material/dialog';
import { MatRippleModule } from '@angular/material/core';

import { IProperty } from '@models/property';

import { ConfirmationModalComponent } from './confirmation-modal.component';
import { fakePropertyPayload } from '../../mocks/properties';
import { provideMockStore } from '@ngrx/store/testing';
import { By } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

class MdDialogMock {
  close() {}
}

const DeletePropertyDialogData: {
  property: IProperty;
} = {
  property: fakePropertyPayload,
};

class StoreMock {
  dispatch(action: any) {}

  select() {
    return of([
      fakePropertyPayload
    ]);
  }
}

describe('ConfirmationModalComponent', () => {
  let component: ConfirmationModalComponent;
  let fixture: ComponentFixture<ConfirmationModalComponent>;
  let debugElement: DebugElement;
  let nativeElement: any;
  let dialog: MatDialogRef<any>;
  let store: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConfirmationModalComponent],
      providers: [
        { provide: Store, useClass: StoreMock },
        { provide: MatDialogRef, useClass: MdDialogMock },
        { provide: MAT_DIALOG_DATA, useValue: DeletePropertyDialogData },
        provideMockStore(),
      ],
      imports: [MatRippleModule, MatDialogModule],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ConfirmationModalComponent);
        dialog = TestBed.inject(MatDialogRef);
        store = TestBed.inject(Store);
        component = fixture.componentInstance;
        nativeElement = fixture.nativeElement;
        debugElement = fixture.debugElement;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should remove property', () => {
    spyOn(dialog, 'close').and.callThrough();
    spyOn(component, 'remove').and.callThrough();
    spyOn(store, 'dispatch').and.callThrough();


    const removeButton = debugElement.query(
      By.css('button[data-qa=remove-property-button]')
    );
    removeButton.triggerEventHandler('click', null);

    expect(component.remove).toHaveBeenCalled();
    expect(store.dispatch).toHaveBeenCalled();
    expect(dialog.close).toHaveBeenCalled();
  });

  it('should close', () => {
    spyOn(dialog, 'close').and.callThrough();

    const closeButton = debugElement.query(
      By.css('button[data-qa=close-dialog-button]')
    );
    closeButton.triggerEventHandler('click', null);

    expect(dialog.close).toHaveBeenCalled();
  });
});
