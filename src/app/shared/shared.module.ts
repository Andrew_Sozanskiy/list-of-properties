import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

import { ConfirmationModalComponent } from './components/confirmation-modal/confirmation-modal.component';
import { PropertyModalComponent } from './components/property-modal/property-modal.component';

const COMPONENTS = [ConfirmationModalComponent, PropertyModalComponent];

const MODULES = [FormsModule, ReactiveFormsModule, CommonModule];

const MATERIAL_MODULES = [
  MatToolbarModule,
  MatDatepickerModule,
  MatInputModule,
  MatNativeDateModule,
  MatListModule,
  MatButtonModule,
  MatDialogModule,
  MatRippleModule,
];

const MODULES_TO_EXPORT = [...MATERIAL_MODULES, ...MODULES];

@NgModule({
  imports: [...MODULES, ...MATERIAL_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [...COMPONENTS],
  exports: [...MODULES_TO_EXPORT],
})
export class SharedModule {}
