export const fakePropertyPayload = {
  id: 1,
  name: 'great name',
  description: 'Fake description',
};

export const storeValueMock = {
  properties: [
    {
      id: 1,
      name: 'great name',
      description: 'Description',
    },
    {
      id: 2,
      name: 'great name',
      description: 'Description',
    },
    {
      id: 3,
      name: 'great name',
      description: 'Description',
    },
  ],
};
