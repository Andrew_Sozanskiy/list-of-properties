import { Component } from '@angular/core';

@Component({
  selector: 'lop-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor() {
  }
}
