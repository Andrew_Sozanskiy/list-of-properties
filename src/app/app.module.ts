import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';

import { PagesModule } from '@pages/pages.module';
import { propertiesReducer } from '@store/reducers/property.reducer';
import { SharedModule } from '@shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { storageMetaReducer } from '@store/meta-reducers/storageMetaReducer';

const MODULES = [
  BrowserModule,
  AppRoutingModule,
  BrowserAnimationsModule,
  SharedModule,
  PagesModule,
];

const MODULES_FOR_ROOT = [
  StoreModule.forRoot({ properties: propertiesReducer }),
  StoreModule.forRoot({ properties: propertiesReducer }, {
    metaReducers: [storageMetaReducer]
  }),
];

@NgModule({
  declarations: [AppComponent],
  imports: [...MODULES, ...MODULES_FOR_ROOT],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
