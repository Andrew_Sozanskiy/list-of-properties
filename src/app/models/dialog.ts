import { IProperty } from '@models/property';

export interface IUserAction {
  action: Actions;
  property?: IProperty;
}

export enum Actions {
  CREATE = 'create',
  UPDATE = 'update',
}
