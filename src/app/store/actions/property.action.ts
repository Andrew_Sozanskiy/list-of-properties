import { createAction, props } from '@ngrx/store';

import { IProperty } from '@models/property';

export const getAll = createAction('[Properties] Get All');

export const addOne = createAction(
  '[Properties] Add One',
  props<{ payload: IProperty }>()
);

export const updateOne = createAction(
  '[Properties] Update One',
  props<{ payload: IProperty }>()
);

export const removeOne = createAction(
  '[Properties] Remove One',
  props<{ payload: IProperty }>()
);
