import { Action, ActionReducer, createReducer, on } from '@ngrx/store';

import {
  addOne,
  getAll,
  removeOne,
  updateOne,
} from '@store/actions/property.action';

import { IProperty } from '@models/property';

export interface State {
  properties: IProperty[];
}

export const initialState: State = {
  properties: [
    {
      id: 1,
      name: 'First property',
      description: 'Simple description',
    },
    {
      id: 2,
      name: 'Second property',
      description: 'Simple description',
    },
  ],
};

const _propertiesReducer = createReducer(
  initialState,
  on(getAll, state => ({ properties: [...state.properties] })),
  on(addOne, (state, { payload }) => {
    return { properties: [...state.properties, payload] };
  }),
  on(updateOne, (state, { payload }) => {
    const propertyId = state.properties.findIndex(
      property => property.id === payload.id
    );
    state.properties[propertyId] = payload;
    console.log(state);
    return { properties: [...state.properties] };
  }),
  on(removeOne, (state, { payload }) => {
    const propertyId = state.properties.findIndex(
      property => property.id === payload.id
    );
    state.properties.splice(propertyId, 1);
    return { properties: [...state.properties] };
  })
);

export function propertiesReducer(state: State, action: any) {
  return _propertiesReducer(state, action);
}
